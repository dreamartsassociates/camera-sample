//
//  main.m
//  CameraSample
//
//  Created by k_nagadou on 2015/04/14.
//  Copyright (c) 2015年 DAAssosiates. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
