//
//  ViewController.h
//  CameraSample
//
//  Created by k_nagadou on 2015/04/14.
//  Copyright (c) 2015年 DAAssosiates. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController<AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic, strong) AVCaptureSession* session;
@property (nonatomic, strong) IBOutlet UIImageView* imageView;

@end
