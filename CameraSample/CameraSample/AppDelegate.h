//
//  AppDelegate.h
//  CameraSample
//
//  Created by k_nagadou on 2015/04/14.
//  Copyright (c) 2015年 DAAssosiates. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

